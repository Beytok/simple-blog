<?php
//include components
include_once 'components/Autoloader.php';
include_once 'config/config.php';
include_once "vendor/autoload.php";

$router = new Router();
$router->run();