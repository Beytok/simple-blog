<?php

class Validator
{
    //array of errors
    static public $errors = array(
        'name' => 'Name cannot be empty.',
        'title' => 'Title cannot be empty.',
        'content' => 'Post cannot be empty.',
        'short_content' => 'Post must be bigger than 10 character.'
    );
    //check if empty
    static function valEmpty($val)
    {
        if (trim($val) === '') {
            return true;
        } else {
            return false;
        }
    }
    //check text length
    static function shortText($val)
    {
        $val = (string)$val;
        if (strlen($val) <= 10) {
            return true;
        }
        return false;
    }
}