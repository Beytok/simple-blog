<?php

class Router
{
    private $routes;

    public function __construct()
    {
        // add routes
        $routesPath = 'config/routes.php';
        $this->routes = include_once($routesPath);
    }

    /**
     * get uri,
     * set controller
     * and action to use
     */
    public function run()
    {
        $uri = trim($_SERVER['REQUEST_URI']);

        foreach ($this->routes as $pattern => $path) {
            //search in uri
            if (preg_match("~$pattern~", $uri)) {
                //if not home page
                if ($pattern !== '/') {
                    $path = preg_replace("~$pattern~", $path, $uri);
                }
                //get uri string to array
                $segment = explode('/', ltrim($path, '/'));
                //set controller and action
                $controllerName = ucfirst(array_shift($segment)) . 'Controller';
                $actionName = array_shift($segment) . 'Action';
                //everything else is parameters
                $parameters = $segment;
                //include controller file
                $controllerFile = 'controllers/' . $controllerName . '.php';
                if (file_exists($controllerFile)) {
                    include_once ($controllerFile);
                }

                $controllerObject = new $controllerName;
                //call controller action and parameters if they are
                call_user_func_array(array($controllerObject, $actionName), $parameters);
                break;
            }
        }
    }
}