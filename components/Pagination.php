<?php

class Pagination
{
    protected $blog;
    //start and pages of pagination
    protected $start;
    protected $end;
    //range pages of pagination
    protected $range;

    public function __construct(Blog $catalog)
    {
        $this->blog = $catalog;
    }

    /**
     * set range of pages
     * @return int
     */
    public function setRange()
    {
        if ($this->blog->visiblePage % 2 === 0) {
            $range = $this->blog->visiblePage / 2;
        } else {
            $range = (($this->blog->visiblePage - 1) / 2);
        }

        return $this->range = $range;
    }

    /**
     * creates pages,
     * and links
     * @param $page
     * @param null $text
     * @return string
     */
    protected function generateHtml($page, $text = null)
    {
        if (!$text) {
            $text = $page;
        }
        $currentURI = rtrim($_SERVER['REQUEST_URI'], "/") . "/";
        $currentURI = preg_replace('~/page-[0-9]+~', '', $currentURI);

        return '<li><a href="' . $currentURI . 'page-' . $page . '">' . $text . '</a></li>';
    }

    /**
     * pagination logic
     */
    protected function setPaginationLogic()
    {
        //if page is 1
        if (($this->blog->page - $this->setRange()) <= 1) {
            $start = 1;
            $end = $this->blog->visiblePage;
            //if last page
        } elseif ($this->blog->controlRows <= $this->blog->showOnPage) {
            $start = $this->blog->page - ($this->blog->visiblePage - 1);
            $end = $this->blog->page;
            //if coming to end
        } elseif ($this->blog->controlRows <= $this->blog->showOnPage * 2) {
            $start = $this->blog->page - $this->range;
            $end = $this->blog->page + 1;
        } else {
            $start = $this->blog->page - $this->range;
            $end = $this->blog->page + $this->range;
        }
        $this->start = $start;
        $this->end = $end;
    }

    /**
     * @return string
     */
    public function setPagination()
    {
        $this->setPaginationLogic();

        $html = '<ul class="pagination">';
        $pagination = '';

        //set active page and not
        for ($page = $this->start; $page <= $this->end; $page++) {
            if (($page > 0) && ($this->blog->controlRows != 0)) {
                if ($page == $this->blog->page || empty($this->blog->page) && $page === 1) {
                    $pagination .= "<li class='active'><a>" . $page . "</a></li>";
                } else {
                    $pagination .= $this->generateHtml($page);
                }
            }
        }
        //set < > in pagination
        if (!empty($pagination)) {
            if ($this->blog->page > 1) {
                $pagination = $this->generateHtml(1, '&lt') . $pagination;
            }
            if ($this->blog->page !== $this->end) {
                $pagination .= $this->generateHtml($this->end, '&gt');
            }
        }
        $html .= $pagination . '</ul>';
        return $html;
    }
}