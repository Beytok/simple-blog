<?php
//set smarty template dirs
class SmartyTemplate
{
    static function getSmarty(){
        $smarty = new \Smarty();
        $smarty->setTemplateDir(SMARTY_TEMPLATE_DIR);
        $smarty->setCacheDir(SMARTY_CACHE_DIR);
        $smarty->setCompileDir(SMARTY_COMPILE_DIR);
        return $smarty;
    }
}