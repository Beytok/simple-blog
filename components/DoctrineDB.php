<?php

use Doctrine\DBAL\DriverManager;

class DoctrineDB
{
    /**
     * connect to database
     * @return \Doctrine\DBAL\Connection
     * @throws \Doctrine\DBAL\DBALException
     */
    public function conn()
    {
        $conn = DriverManager::getConnection(array(
            'dbname' => DB_NAME,
            'user' => DB_USER,
            'password' => DB_PASS,
            'host' => DB_HOST,
            'driver' => DB_DRIVER
        ));
        return $conn;
    }
}