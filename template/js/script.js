$(document).ready(function () {
    setTimeout(function () {
        $('#logo p').slideUp(2000)
    }, 5000);

    $('#menu a').hover(function () {
        $(this).animate({fontSize: 14}, 0);
    }, function () {
        $(this).animate({fontSize: 12}, 0);
    });

    $('#submit').hover(function () {
        $(this).css('backgroundColor', '#9F1615');
    }, function () {
        $(this).css('backgroundColor', '#DA2F27');
    });
});