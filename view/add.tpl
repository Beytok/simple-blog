{extends file="layot.tpl"}
{block 'container'}
    <div id="page">
        <div id="page-bgbtm">
            <div id="content">
                <div id="form">
                    <h2 class="required">{$page_title}</h2>
                    <!--if errors array not empty displays error-->
                    {if $errors != false}
                        <ul id="error">
                            {foreach $errors as $error}
                                <li id="red_text">{$error}</li>
                            {/foreach}
                        </ul>
                    {/if}

                    <form action="" method="post" class="required">
                        <div>
                            <label for="form_name" class="required">Your name</label>
                            <input type="text" id="form_name" name="name" class="placeholder" placeholder="Your name"
                                   value="{$name}"/>
                        </div>

                        <div>
                            <label for="form_title" class="required" ">Title</label>
                            <input type="text" id="form_title" name="title" class="placeholder" placeholder="Title"
                                   value="{$title}"/>
                        </div>

                        <div>
                            <label for="form_text" class="required">Post</label>
                            <textarea id="form_text" name="content" required="required"
                                      placeholder="Your post">{$content}</textarea>
                        </div>
                        <input id="submit" type="submit" name="submit" value="Post">

                    </form>
                </div>
            </div>
        </div>
    </div>
{/block}