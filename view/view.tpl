{extends file="layot.tpl"}
{block 'container'}
    <div id="page">
        <div id="page-bgbtm">
            <div id="content">
                <div class="post">
                    <h2 class="title"><a href='/view/{$postById.id}'>{$postById.title}</a></h2>

                    <p class="meta">Posted by <span id="red_text">{$postById.name}</span>
                        on {$postById.time}</p>

                    <p>{nl2br($postById.content)}</p>

                    <span id="left"><a href='/'><< Back to HomePage</a></span>
                </div>
            </div>
        </div>
    </div>
{/block}