{extends file="layot.tpl"}
{block 'container'}
    <div id="page">
        <div id="page-bgbtm">
            <div id="content">
                <!--display all posts from array-->
                {foreach $allPosts as $post}
                    <div class="post">
                        <h2 class="title"><a href='/view/{$post.id}'>{$post.title}</a></h2>

                        <p class="meta">Posted by <span id="red_text">{$post.name}</span> on {$post.time}
                            &nbsp;&bull;&nbsp; <a href='/view/{$post.id}' class="permalink"> Full article</a></p>

                        <div class="entry">
                            <p>{$post.short_content}</p>
                        </div>
                    </div>
                {/foreach}
                <div class="pagination">
                    {$pagination}
                </div>
            </div>
        </div>
    </div>
{/block}