<!DOCTYPE html>
<html>
<head>
    {block 'head'}{/block}
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <title>{block "title"}{$page_title}{/block}</title>
    <link href="/template/css/style.css" rel="stylesheet" type="text/css" media="screen"/>
    <script src="/template/js/jquery-3.1.1.min.js"></script>
    <script src="/template/js/script.js"></script>
</head>
<body>
{block 'header'}
<div id="wrapper">
    <div id="menu-wrapper">
        <div id="menu">
            <!--current page if title same with page-->
            <ul>
                <li {if $page_title == 'Home page'}class="current_page_item"{/if}><a href="/">Home page</a></li>
                <li {if $page_title == 'Add post'}class="current_page_item"{/if}><a href="/add">Add post</a></li>
            </ul>
        </div>
    </div>
    <div id="header-wrapper">
        <header id="header">
            <div id="logo">
                <h1><a href="/">Blog</a></h1>

                <p>by Vitaliy Smolinskiy</p>
            </div>
        </header>
    </div>
    {/block}
    {block 'container'}{/block}
    {block 'footer'}
</div>
    <footer id="footer">
        <p>&copy; 2016 - {date('Y')}  by Vitaliy Smolinskiy.</p>
    </footer>
{/block}
</body>
</html>