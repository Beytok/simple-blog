<?php
// array of routes use in blog
return [
    'view/([0-9]+)' => 'blog/view/$1',
    'add' => 'blog/add',
    'page-([0-9]+)' => 'blog/index/$1',
    '/' => 'blog/index'
];