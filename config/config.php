<?php
//database properties in constants
define("DB_HOST", "localhost");
define("DB_USER", "root");
define("DB_PASS", "root");
define("DB_NAME", "my_blog");
define("DB_DRIVER", "pdo_mysql");
//smarty dirs in constants
define("SMARTY_TEMPLATE_DIR", "view/");
define("SMARTY_CACHE_DIR", "cache/");
define("SMARTY_COMPILE_DIR", "templates_c/");