<?php


class BlogController
{
    private $blog;
    private $smarty;
    // set smarty and model
    public function __construct()
    {
        $blog = new Blog();
        $this->blog = $blog;

        $smarty = SmartyTemplate::getSmarty();
        $this->smarty = $smarty;
    }

    /**
     * displays home page
     * @param int $page
     */
    public function indexAction($page = 1)
    {
        $allPosts = $this->blog->getPosts($page);
        $this->smarty->assign('allPosts', $allPosts);
        $this->smarty->assign('pagination', $this->blog->getPagination());

        $this->setTitle('Home page');
        $this->smarty->display('index.tpl');
    }

    /**
     * displays view post
     * @param $id
     */
    public function viewAction($id)
    {
        $id = (int)$id;
        $postById = $this->blog->getPostById($id);
        $this->smarty->assign('postById', $postById);

        $this->setTitle('View post');
        $this->smarty->display('view.tpl');
    }

    /**
     * display add form
     * check valid post
     * add to database
     */
    public function addAction()
    {
        $name = '';
        $title = '';
        $content = '';
        $errors = false;

        if (isset($_POST['submit'])) {
            $name = htmlspecialchars($_POST['name']);
            $title = htmlspecialchars($_POST['title']);
            $content = htmlspecialchars($_POST['content']);

            if (Validator::valEmpty($name)) {
                $errors['name'] = Validator::$errors['name'];
            }
            if (Validator::valEmpty($title)) {
                $errors['title'] = Validator::$errors['title'];
            }
            if (Validator::shortText($content)) {
                $errors['short_content'] = Validator::$errors['short_content'];
            }

            if ($errors === false) {
                $this->blog->addPost(array(
                    'name' => $name,
                    'title' => $title,
                    'content' => $content
                ));

                header('Location: /');
            }
        }
        $this->smarty->assign('name', $name);
        $this->smarty->assign('title', $title);
        $this->smarty->assign('content', $content);
        $this->smarty->assign('errors', $errors);

        $this->setTitle('Add post');
        $this->smarty->display('add.tpl');
    }

    /**
     * set title var for pages
     * @param $title
     */
    public function setTitle($title)
    {
        $this->smarty->assign('page_title', $title);
    }

}