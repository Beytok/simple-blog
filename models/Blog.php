<?php

class Blog
{
    private $DoctrineDB;
    //option for short content
    private $wordsReturned = 35;
    //option for pagination
    public $visiblePage = 3;
    public $showOnPage = 3;
    public $page;
    public $controlRows;

    public function __construct()
    {
        //connect database
        $db = new DoctrineDB();
        $this->DoctrineDB = $db;
    }

    /**
     * string of pagination
     * @return string
     */
    public function getPagination()
    {
        $pagination = new Pagination($this);
        return $pagination->setPagination();
    }

    /**
     * get all posts for home page
     * @param $page
     * @return array
     */
    public function getPosts($page)
    {
        $this->page = $page;

        $firstPage = abs(($page - 1) * $this->showOnPage);

        $posts = $this->DoctrineDB
            ->conn()
            ->fetchAll(
                $this->DoctrineDB->conn()
                    ->createQueryBuilder()
                    ->select('id, name, title, short_content, time')
                    ->from('blog')
                    ->setFirstResult($firstPage)
                    ->setMaxResults($this->showOnPage * 2 + 1)
            );

        $this->controlRows = count($posts);
        //if array of posts bigger than we need cut him
        if ($this->controlRows > $this->showOnPage) {
            $posts = array_slice($posts, 0, $this->showOnPage);
        }

        return $posts;
    }

    /**
     * get one post for view
     * @param $id
     * @return array
     */
    public function getPostById($id)
    {
        $postById = $this->DoctrineDB
            ->conn()
            ->fetchAssoc(
                $this->DoctrineDB->conn()
                    ->createQueryBuilder()
                    ->select('id,name,title,content,time')
                    ->from('blog')
                    ->where('id = ?'), array($id)
            );
        return $postById;
    }

    /**
     * add post to database,
     * create short content,
     * @param array $options
     * @return \Doctrine\DBAL\Driver\Statement
     * @throws \Doctrine\DBAL\DBALException
     */
    public function addPost(array $options)
    {
        $conn = $this->DoctrineDB->conn();
        $query = $conn->createQueryBuilder()
            ->insert('blog')
            ->setValue('name', '?')
            ->setValue('title', '?')
            ->setValue('short_content', '?')
            ->setValue('content', '?');

        return $conn->executeQuery((string)$query, array(
            $options['name'],
            $options['title'],
            $this->setShortContent(
                $options['content'],
                $this->wordsReturned),
            $options['content']
        ));

    }

    /**
     * @param $content
     * @param $wordsReturned
     * @return mixed|string
     */
    public function setShortContent($content, $wordsReturned)
    {
        $origin = $content;
        //change all symbols to space
        $content = preg_replace('/(?<=\S,)(?=\S)/', ' ', $content);
        //change enter to space
        $content = str_replace("\n", " ", $content);
        //parsed content
        $arrayContent = explode(" ", $content);
        if (count($arrayContent) <= $wordsReturned) {
            return $origin;
        } else {
            array_splice($arrayContent, $wordsReturned);
            $content = implode(" ", $arrayContent) . " ...";
        }
        return $content;
    }

}